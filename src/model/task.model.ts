export class Task {
    public id: number;
    public title: string;
    public description: string;
    public dueDate: Date;
    public piority: string;
}

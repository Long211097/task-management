import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { Task } from '../../model/task.model';
import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.css'],
})
export class FormEditComponent implements OnInit {
  @ViewChild('title') input;
  public currentDate = new Date();
  public dataTask: Task = {
    id: 0,
    title: '',
    description: '',
    dueDate: new Date(),
    piority: 'normal',
  };
  @Input() type = 'create';
  @Input() dataEdit: Task;
  @Output() public created = new EventEmitter();
  @Output() public updated = new EventEmitter();
  ngOnInit(): void {
    if (this.type === 'update') {
      this.dataTask = this.dataEdit;
    }
  }

  public save = () => {
    if (this.validation()) {
      this.type === 'create' ? this.create() : this.update();
    }
  }

  public create = () => {
    if (this.dataTask.title === '') {
      alert('Task title is a required field.');
      return;
    }
    this.dataTask.id = Math.random();
    const data = JSON.parse(localStorage.getItem('dataTasks'));
    if (data) {
      data.push(this.dataTask);
      localStorage.setItem('dataTasks', JSON.stringify(data));
    } else {
      localStorage.setItem('dataTasks', JSON.stringify([this.dataTask]));
    }
    this.created.emit();
  }

  public update = () => {
    let data = JSON.parse(localStorage.getItem('dataTasks'));
    data = data.map(
      (obj) => [this.dataTask].find((o) => o.id === obj.id) || obj
    );
    localStorage.setItem('dataTasks', JSON.stringify(data));
    this.updated.emit();
  }

  public validation = () => {
    if (this.dataTask.title === '') {
      alert('Task title is a required field.');
      this.input.nativeElement.focus();
      return false;
    }
    return true;
  }
}

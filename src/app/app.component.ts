import { Component, OnInit } from '@angular/core';
import { Task } from 'src/model/task.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public dataTask: Task[];
  public selectedTask: number[] = [];
  ngOnInit(): void {
    this.loadData();
  }

  public loadData = () => {
    const data =  JSON.parse(localStorage.getItem('dataTasks'));
    if (data) {
      this.dataTask = data.sort((a, b) => new Date(a.dueDate).getTime() - new Date(b.dueDate).getTime());
    }
  }

  public selected = (e: any) => {
    if (e.checked) {
      this.selectedTask.push(e.id);
    } else {
      this.selectedTask = this.selectedTask.filter(x => x !== e.id);
    }
  }

  public done = () => {
    alert('comming soon ...');
  }

  public multiDetele = () => {
    let data =  JSON.parse(localStorage.getItem('dataTasks'));
    data = data.filter(i => !this.selectedTask.some(j => j === i.id));
    localStorage.setItem('dataTasks', JSON.stringify(data));
    this.selectedTask = [];
    this.loadData();
  }

  public search = (e: string) => {
    const dataSearch =  JSON.parse(localStorage.getItem('dataTasks'));
    this.dataTask = dataSearch.filter(x => x.title.includes(e));
  }
}

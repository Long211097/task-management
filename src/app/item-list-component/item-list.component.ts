import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/model/task.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  public showDetail = false;
  public checked = false;
  @Input() data: Task = {
    id : 0,
    title : '',
    description: '',
    dueDate: new Date(Date.now()),
    piority: 'low'
  };
  @Output() deletedEvent = new EventEmitter();
  @Output() updatedEvent = new EventEmitter();
  @Output() selectedEvent = new EventEmitter();
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  public deteted = (id: number) => {
    let data =  JSON.parse(localStorage.getItem('dataTasks'));
    data = data.filter(x => x.id !== id);
    localStorage.setItem('dataTasks', JSON.stringify(data));
    this.deletedEvent.emit();
  }
  public updated = () => {
    this.updatedEvent.emit();
  }

  public selected = (id: number) => {
    this.checked = !this.checked;
    this.selectedEvent.emit({ id, checked: this.checked });
  }
}
